#include <iostream>
#include <math.h>
#include <SDL2/SDL.h>
#define GL_GLEXT_PROTOTYPES 1
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>

#include "overlay.h"
#include "circle.h"

#define WIDTH 800
#define HEIGHT 600

#define DIM_N 1000
#define DIM_M 10
#define RADIUS 100.0f
#define SIZE_X 0.3f
#define SIZE_Y 0.5f
#define SIZE_Z 0.7f
#define COLOR 0.75f

#define HI 1.0f
#define LO 0.8f

#define CLEAR_A 0.2f

#define T_RESET 5000

static SDL_Window * window = NULL;
static SDL_GLContext gl_context;

static bool done = false;
static float angle = 1.0f;
static int last_time_reset = 0;
static int last_time_increase = 0;
static float v1[3]= {1.0f, 1.0f, 1.0f};
static float v2[3]= {-1.0f, 1.0f, -1.0f};
static float v[3];
static float offset = 0.0f;
static float size_x = SIZE_X;
static float size_y = SIZE_Y;
static float size_z = SIZE_Z;
static float toggle = 0;
static bool reset = false;
static bool increase = false;
static float random_time = 0.0f;
static float clear_x = 0.9f;
static float clear_y = 0.9f;
static float clear_z = 0.9f;
int delta = 0;

#define LOGO "./assets/logo.bmp"

static circle * c = NULL;

int SDLCALL watch(void *userdata, SDL_Event* event) {

	if (event->type == SDL_APP_WILLENTERBACKGROUND) {
		done = 1;
	}

	return 1;
}

void init()
{
	glMatrixMode(GL_PROJECTION);                    // Select The Projection Matrix
	glLoadIdentity();                               // Reset The Projection Matrix
	glOrtho(-10, 5, -5, 5, -50, 50);
	glMatrixMode(GL_MODELVIEW);                     // Select The Modelview Matrix
	glLoadIdentity();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glPointSize(5.0f);
	glEnable(GL_FOG);
	glFogf(GL_FOG_START, -500);
	glFogf(GL_FOG_END, 500);
	glFogf(GL_FOG_DENSITY, 0.005);
}

void update()
{
	int current_time = SDL_GetTicks();

	delta = current_time - last_time_reset;

	if (toggle)
	{
		v[0] = v1[0];
		v[1] = v1[1];
		v[2] = v1[2];
	}
	else
	{
		v[0] = v2[0];
		v[1] = v2[1];
		v[2] = v2[2];
	}

	// increase or decrease randomly
	if ((float)(last_time_increase + random_time) < (float)current_time) {
		last_time_increase = current_time;
		increase = (bool)(rand() % 2);
	}

	// reset
	if (last_time_reset + T_RESET < current_time) {
		last_time_reset = current_time;
		reset = true;
		random_time = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/T_RESET));
		angle = 1.0f;
		offset = 0.0f;
		toggle = !toggle;
		clear_x = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
		clear_y = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
		clear_z = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
	}
	else
	{
		angle = angle + 0.00001f;
		if (increase) {
			offset += 0.001f;
			size_x -= 0.01f;
			size_y -= 0.05f;
			size_z *= 0.1f;
		}
		else {
			offset -= 0.0001f;
			size_x += 0.001f;
			size_y += 0.005f;
			size_z *= 0.01f;
		}
		reset = false;
	}
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor4ub(255, 255, 255, 255);

	float ix = RADIUS * cos(angle);
	float iy = RADIUS * sin(angle);
	float iz = atan2(ix, iy) + PI / 2;

#define TOL (80 * RADIUS / 100)
	glTranslatef(ix, iy, 0.0); // 3. Translate to the object's position.
	glRotatef(angle, 1.0, 0.0, 0.0); // 2. Rotate the object.
	glTranslatef(-ix, -iy, 0.0); // 1. Translate to the origin.

	if (reset)
		c->reset();

	c->update();
	c->render();
}

int main(int argc,char *argv[])
{
	circle cc(DIM_N, DIM_M, 5.0f, SIZE_X, SIZE_Y, SIZE_Z, COLOR, RADIUS);
	c = &cc;
	c->reset();
	overlay o(LOGO, 25, 525, T_RESET, WIDTH);

	window = SDL_CreateWindow("5bdj", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP);

	gl_context = SDL_GL_CreateContext(window);

	init();

	SDL_AddEventWatch(watch, NULL);

	while(!done) {
		SDL_Event event;
		while( SDL_PollEvent(&event) ) {
			if((event.type == SDL_QUIT) || (SDLK_ESCAPE == event.key.keysym.sym)) {
				done = 1;
			}
		}

		// update parameters
		update();

		// clear the screen again , different color
		glClearColor(clear_x, clear_y, clear_z, 0.5f);
		glClear(GL_COLOR_BUFFER_BIT);

		// 3d
		render();

		// switch context
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();

		glOrtho(-10.0, WIDTH, HEIGHT, 0.0, -1.0f, 1.0f);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

		// draw 2d overlay
		o.render(delta);

		// end 2d
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glEnable(GL_DEPTH_TEST);

		SDL_GL_SwapWindow(window);
	}

	SDL_DelEventWatch(watch, NULL);
	SDL_GL_DeleteContext(gl_context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
