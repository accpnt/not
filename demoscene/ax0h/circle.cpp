#include <iostream>
#include <array>
#include <math.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "circle.h"
#include "ctx.h"
#include "matrix.h"

#define PADDING 10.0f

#define HI 1.0f
#define LO 0.9f

circle::circle(int rows, int columns, float radius, float size_x, float size_y, float size_z, float color, float offset)
{
	_rows = rows;
	_columns = columns;
	_nb_cuboids = rows * columns;
	_offset = offset;
	_radius = radius;
	_theta = 2 * PI / rows;

	// initialize pointers
	_ctx = std::unique_ptr<ctx>(new ctx("lua/matrix.lua", rows, columns));
	_cuboids.reset( new std::unique_ptr<cuboid>[_nb_cuboids] );

	for(unsigned int i = 0; i < _nb_cuboids; i++) {
		bool lines = (bool)(rand() % 2);
		std::array<float, 3> size = {size_x, size_y, size_z};
		_cuboids[i].reset( new cuboid(size, color, lines) );
	}
}

void circle::reset()
{
	for(unsigned int i = 0; i < _rows; i++) {
		for(unsigned int j = 0; j < _columns; j++) {
			_cuboids[i]->reset();
		}
	}

	_ctx->reset();
	_z = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI - LO)));
}

void circle::update()
{
	_ctx->update();
}

void circle::render()
{
	for(unsigned int i = 0; i < _rows; i++) {
		float a = _theta * i;
		float ix = _radius * cos(a);
		float iy = _radius * sin(a);
		for (unsigned int j = 0; j < _columns; j++) {
			glPushMatrix();
				_cuboids[i * _columns + j]->set_coords(i + atan2(iy, ix) - PI / 2 + j, ix, iy);
				_cuboids[i * _columns + j]->render();
			glPopMatrix();
		}
	}

}
