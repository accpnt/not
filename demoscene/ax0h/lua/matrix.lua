p = {
	-- configuration
	size_x = 0.3,
	size_y = 0.5,
	size_z = 0.7,
	color = 0.75,

	-- transformation parameters
	amplitude = 0.0001,
	frequency = 0.0001,
	phase = 0.0,
	dampening = 0.0000001;
}

previous = {}
sign = -1

-- print matrix
function mp(m)
	for i = 1, #m do
		for j = 1, #m[i] do
			local s = tostring(m[i][j])
			io.write(s, " ")
		end
		io.write('\n')
	end
end

-- pseudo-random numbers
local A1, A2 = 727595, 798405  -- 5^17=D20*A1+A2
local D20, D40 = 1048576, 1099511627776  -- 2^20, 2^40
local X1, X2 = 0, 1

function rand()
    local U = X2*A2
    local V = (X1*A2 + X2*A1) % D20
    V = (V*D20 + U) % D40
    X1 = math.floor(V/D20)
    X2 = V - X1*D20
    return V/D40
end


-- initialize parameters
function init(m)
	p.amplitude = 0.001
	p.frequency = 0.0000001
	p.phase = rand()

	r = {}
	for i = 1, #m do
		r[i] = {}
		for j = 1, #m[i] do
			r[i][j] = 1.0
		end
	end
	return r
end

-- copy matrix
function copy(m)
	c = {}

	for i = 1, #m do
		c[i] = {}
		for j = 1, #m[i] do
			c[i][j] = m[i][j]
		end
	end
	return c
end

-- transform input matrix
function transform(m)
	r = {}

	for i = 1, #m do

		r[i] = {}

		if (i % 2 == 0) then
			sign_i = -1
		else
			sign_i = 1
		end

		for j = 1, #m[i] do
			if (j % 2 == 0) then
				sign_j = -1
			else
				sign_j = 1
			end

			r[i][j] = math.sin(i * p.frequency) * i  * p.amplitude + math.tan(j * p.frequency) * j

			p.amplitude = p.amplitude + 0.00005
			p.frequency = p.frequency + 0.000005
			p.phase = p.phase + 0.1
		end
	end
	return r
end

function reset_conf()
	-- reset configuration
	p.size_x = rand()
	p.size_y = rand()
	p.size_z = rand()
	p.color = 0.75
end

function update_conf()
	if (1.0 < p.size_x or 1.0 < p.size_y or 1.0 < p.size_z) then
		reset_conf()
	else
		p.size_x = p.size_x + 0.0005
		p.size_y = p.size_y + 0.0005
		p.size_z = p.size_z + 0.0005
		p.color = p.color + 0.0005
	end
end

-- called by c++
function reset()
	io.write("resetting matrix...\n")
	m = pull()
	r  = init(m)
	push(r)

	reset_conf()

	-- reset transformation parameters
	p.amplitude = 0.0001
	p.frequency = 0.0001
	p.phase = 0.0
end

-- called by c++
function update()
	update_conf()

	previous = pull()
	transformed = transform(previous)
	push(transformed)
end
