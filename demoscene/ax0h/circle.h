#ifndef __CIRCLE__
#define __CIRCLE__

#include <string>
#include <memory>

#include "cuboid.h"
#include "ctx.h"

#define PI 3.1416

class circle
{
	public:
		circle(int rows, int columns, float radius, float size_x, float size_y, float size_z, float color, float offset);
		void update();
		void render();
		void reset();

	private:
		unsigned int _nb_cuboids;
		unsigned int _rows;
		unsigned int _columns;
		std::unique_ptr<std::unique_ptr<cuboid>[]> _cuboids;
		std::unique_ptr<float[]> _coords;
		float _offset;
		std::unique_ptr<ctx> _ctx;
		int _z;
		float _radius;
		float _theta;
};

#endif  // __CIRCLE__
