#include <array>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "dodecahedron.h"

#define PSI  (1.618)
#define CUBES  (12)

float coords[CUBES][3] =
{
	{PSI,1,0}, {1,0,PSI}, {0,PSI,1}, {PSI,-1,0},
	{-1,0,PSI}, {0,PSI,-1}, {-PSI,1,0}, {1,0,-PSI},
	{0,-PSI,1}, {-PSI,-1,0}, {-1,0,-PSI}, {0,-PSI,-1}
};

dodecahedron::dodecahedron(float size, float color, float offset)
{
	_nb_cubes = CUBES;
	_offset = offset;
	_cubes.reset( new std::unique_ptr<cuboid>[CUBES] );

	for(int i = 0; i < CUBES; i++) {
		bool lines = (bool)(rand() % 2);
		std::array<float, 3> asize = {size / 2, size / 2, size / 2};
		_cubes[i].reset( new cuboid(asize, color, lines) );
	}
}

void dodecahedron::update(float size, float color, float offset)
{
	_size = size;
	_color = color;
	_offset = offset;
}

void dodecahedron::render()
{
	float offset = _offset;

	for (unsigned int i = 0; i < _nb_cubes; i++)
	{
		offset += 0.1;
		_cubes[i]->set_offset(offset);
		std::array<float, 3> size = {_size + offset, _size + offset, _size + offset};

		glPushMatrix();
			glTranslatef(coords[i][0], coords[i][1], coords[i][2]);
			glColor3ub(0,255,0);
			_cubes[i]->update(size, _color + offset);
			_cubes[i]->render();
		glPopMatrix();
	}
}
