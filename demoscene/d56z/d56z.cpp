#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "dodecahedron.h"
#include "overlay.h"

#define WIDTH 800
#define HEIGHT 600

#define SIZE 0.5f
#define COLOR 0.2f

#define T_RESET 5000

static int done = 0;
static float angle = 1.0f;
int delta = 0;
static int lastTime = 0;
static float v1[3]= {1.0f, 1.0f, 1.0f};
static float v2[3]= {-1.0f, 1.0f, -1.0f};
static float v[3];
static float offset = 0.0f;
static int toggle = 0;
static SDL_Window * window = NULL;
static SDL_GLContext gl_context;

#define LOGO "./assets/logo.bmp"

static dodecahedron * d = NULL;

int SDLCALL watch(void *userdata, SDL_Event* event) {

	if (event->type == SDL_APP_WILLENTERBACKGROUND) {
		done = 1;
	}

	return 1;
}

void init()
{
	glMatrixMode(GL_PROJECTION);                    // Select The Projection Matrix
	glLoadIdentity();                               // Reset The Projection Matrix
	glOrtho(-5, 5, -5, 5, -50, 50);
	glMatrixMode(GL_MODELVIEW);                     // Select The Modelview Matrix
	glLoadIdentity();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
	glPointSize(5.0f);
	glEnable(GL_FOG);
	glFogf(GL_FOG_START, -500);
	glFogf(GL_FOG_END, 500);
	glFogf(GL_FOG_DENSITY, 0.005);
}

void update()
{
	int currentTime = 0;

	if (toggle > 100000) {
		v[0] = v1[0];
		v[1] = v1[1];
		v[2] = v1[2];
		toggle = 0;
	}
	else {
		v[0] = v2[0];
		v[1] = v2[1];
		v[2] = v2[2];
	}

	if(1.0f < offset)
	{
		offset = 0.0f;
	}

	SDL_Delay(100);

	// rotation angle
	angle = angle + 0.1f;
	if(10.0f < angle)
	{
		angle = 1.0f;
	}

	currentTime = SDL_GetTicks();

	delta = currentTime - lastTime;

	if (lastTime + 10000 < currentTime) {
		lastTime = currentTime;
		toggle++;
		offset += 0.05;
	}
}

void render()
{
	// update parameters
	update();

	SDL_GL_MakeCurrent(window, gl_context);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor4ub(255, 255, 255, 255);

	glRotatef(angle, v[0], v[1], v[2]);
	d->update(SIZE + offset, 1.0f - offset, offset);
	d->render();
}

int main(int argc,char *argv[])
{
	dodecahedron sd(SIZE, COLOR, 0.0f);
	d = &sd;
	overlay o(LOGO, 25, 525, T_RESET, WIDTH);

	window = SDL_CreateWindow("d56z", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP);

	gl_context = SDL_GL_CreateContext(window);

	init();

	SDL_AddEventWatch(watch, NULL);

	while(!done) {
		SDL_Event event;
		while( SDL_PollEvent(&event) ) {
			if((event.type == SDL_QUIT) || (SDLK_ESCAPE == event.key.keysym.sym)) {
				done = 1;
			}
		}

		/* Clear the screen again , different color */
		glClearColor(0.9f, 0.9f, 0.9f, 0.5f);
		glClear(GL_COLOR_BUFFER_BIT);


		render();

		// switch context
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();

		glOrtho(0.0, 800, 600, 0.0, -1.0f, 1.0f);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

		// draw 2d overlay
		o.render(delta);

		//End 2D
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glEnable(GL_DEPTH_TEST);

		SDL_GL_SwapWindow(window);
	}

	SDL_DelEventWatch(watch, NULL);
	SDL_GL_DeleteContext(gl_context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
