#ifndef __DODECAHEDRON__
#define __DODECAHEDRON__

#include <string>
#include <memory>

#include "cuboid.h"

class dodecahedron
{
	public:
		dodecahedron(float size, float color, float offset);
		void update(float size, float color, float offset);
		void render();

	private:
		unsigned int _nb_cubes;
		std::unique_ptr<std::unique_ptr<cuboid>[]> _cubes;
		float _size;
		float _color;
		float _offset;
};

#endif  // __DODECAHEDRON__
