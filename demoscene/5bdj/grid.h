#ifndef __GRID__
#define __GRID__

#include <string>
#include <memory>

#include "cuboid.h"
#include "ctx.h"

class grid
{
	public:
		grid(int dim, float size_x, float size_y, float size_z, float color, float offset);
		void update();
		void render();
		void reset();

	private:
		unsigned int _nb_cuboids;
		unsigned int _dim;
		std::unique_ptr<std::unique_ptr<cuboid>[]> _cuboids;
		std::unique_ptr<float[]> _coords;
		float _offset;
		std::unique_ptr<ctx> _ctx;
		int _z;
};

#endif  // __GRID__
