#include <iostream>
#include <array>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "grid.h"
#include "ctx.h"

#define PI 3.1416
#define MULT 10.0f

#define PADDING 0.5f

#define HI 1.0f
#define LO 0.0f

grid::grid(int dim, float size_x, float size_y, float size_z, float color, float offset)
{
	_dim = dim;
	_nb_cuboids = dim * dim;
	_offset = offset;

	// initialize pointers
	_ctx = std::unique_ptr<ctx>(new ctx("lua/matrix.lua", dim, dim));
	_cuboids.reset( new std::unique_ptr<cuboid>[_nb_cuboids] );
	_coords.reset(new float[_nb_cuboids * 3]);

	for(unsigned int i = 0; i < _nb_cuboids; i++) {
		bool lines = (bool)(rand() % 2);
		std::array<float, 3> size = {size_x, size_y, size_z};
		_cuboids[i].reset( new cuboid(size, color, lines) );
	}
}

void grid::reset()
{
	for(unsigned int i = 0; i < _dim; i++) {
		for(unsigned int j = 0; j < _dim; j++) {
			_cuboids[i]->reset();
		}
	}

	_ctx->reset();
	_z = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI - LO)));
}

void grid::update()
{
	// retrieve lua parameters
	float sx = _ctx->get_param_value("size_x");
	float sy = _ctx->get_param_value("size_y");
	float sz = _ctx->get_param_value("size_z");
	std::array<float, 3> size = {sx, sy, sz};

	float c = _ctx->get_param_value("color");

	for(unsigned int i = 0; i < _dim; i++) {
		for (unsigned int j = 0; j < _dim; j++) {
			_cuboids[i * _dim + j]->update(size, c);
		}
	}

	_ctx->update();
}

void grid::render()
{
	for(unsigned int i = 0; i < _dim; i++) {
		for (unsigned int j = 0; j < _dim; j++) {
			glPushMatrix();
				_cuboids[i * _dim + j]->set_coords(i * PADDING, j * PADDING, _ctx->get_value(i, j));
				_cuboids[i * _dim + j]->render();
			glPopMatrix();
		}
	}
}
