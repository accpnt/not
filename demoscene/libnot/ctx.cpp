#include <iostream>

#include "ctx.h"
#include "matrix.h"

static std::unique_ptr<matrix<float>> spm;

static void _pull(lua_State * _L, int idx)
{
	int i = 0;
	int j = 0;
	int nr = 0;
	int nc = 0;

	if(lua_istable(_L, idx)) {
		nr = lua_rawlen(_L, idx);  // retrieve number of rows

		lua_pushnil(_L);  // first key: iterating the first dimension
		while (lua_next(_L, idx) != 0) {
			nc = lua_rawlen(_L, -1);  // retrieve number of columns

			if(lua_istable(_L, -1)) {
				lua_pushnil(_L);  //first key: iterating the second dimension
				j = 0;  // reset row counter
				while (lua_next(_L, -2) != 0) { // table is in the stack at index -2
					(*spm)(i, j) = lua_tonumber(_L, -1);  // value is at index -1
					lua_pop(_L, 1);  // removes 'value'; keeps 'key' for next iteration
					j++;
				}
			}
			lua_pop(_L, 1);  // removes 'value'; keeps 'key' for next iteration
			i++;
		}
	}

	return;
}

static int _push(lua_State * _L)
{
	unsigned int i = 0;
	unsigned int j = 0;

	lua_newtable(_L);
	for(i = 0; i < spm->get_rows(); i++) {
		lua_pushnumber(_L, i + 1);    // parent table index
		lua_newtable(_L);             // child table
		for(j = 0; j < spm->get_cols(); j++ ) {
			lua_pushnumber( _L, j + 1 );  // this will be the child's index
			lua_pushnumber( _L, (*spm)(i,j));  // this will be the child's value
			lua_settable( _L, -3 );
		}
		lua_settable( _L, -3 );
	}

	return 1;
}


// pull matrix from lua point of view
static int pull(lua_State * _L)
{
	_push(_L);

	return 1;
}

// push matrix from lua point of view
static int push(lua_State * _L)
{
	// get number of arguments
	int n = lua_gettop(_L);

	if(1 == n) {
		_pull(_L, 1);
	}

	return 1;
}

ctx::ctx(const char * script, int rows, int columns)
{
	_m = std::unique_ptr<matrix<float>>(new matrix<float>(rows, columns, 0.0f));
	spm = std::unique_ptr<matrix<float>>(new matrix<float>(rows, columns, 0.0f));

	// initialize lua
	_L = luaL_newstate();
	luaL_openlibs(_L);

	// register functions
	lua_register(_L, "pull", pull);
	lua_register(_L, "push", push);

	load_file(script);

	// store function to be run later
	store();
}

ctx::~ctx()
{
	lua_close(_L);
}

void ctx::get_params(const char * p)
{
	lua_getglobal(_L, p);
	lua_pushnil(_L);

	int i = 0;
	while(lua_next(_L,-2))
	{
		const char * key = lua_tostring(_L,-2);
		double value = lua_tonumber(_L,-1);
		lua_pop(_L,1);
		std::cout << key << " = " << value << std::endl;
		_h[i].key.assign(key);
		_h[i].value = value;
		i++;
	}
	lua_pop(_L, 1);
}

int ctx::retrieve_function(const char * function)
{
	int fidx = -1;  // assume failure by default

	lua_getglobal(_L, function); // retrieve function to store

	if (lua_isfunction(_L, -1)) {
		fidx = luaL_ref(_L, -2); // store a function in the function table
	}
	else {
		std::cout << "can't find " << function << std::endl;
	}

	return fidx;
}

void ctx::store()
{
	lua_newtable(_L);  // create table for functions
	_idx = luaL_ref(_L, LUA_REGISTRYINDEX); // store said table in pseudo-registry
	lua_rawgeti(_L, LUA_REGISTRYINDEX, _idx); // retrieve table for functions

	_u = retrieve_function("update");
	_r = retrieve_function("reset");

	// table is two places up the current stack counter
	lua_pop(_L, 1); // we are done with the function table, so pop it

	std::cout << "function table idx: " << _idx << std::endl;
}

void ctx::run(int fidx)
{
	int status;

	if (fidx == -1) {
		std::cout << "invalid function index " << fidx << std::endl;
	}
	else {
		lua_rawgeti(_L, LUA_REGISTRYINDEX, _idx); // retrieve function table
		lua_rawgeti(_L, -1, fidx); // retrieve function
		//use function
		status = lua_pcall(_L, 0, 0, 0);  // 0 arguments, 0 results
		if (status != LUA_OK) {
			std::cout << "error running function" << lua_error(_L) << std::endl;
		}
		//don't forget to pop the function table from the stack
		lua_pop(_L, 1);
	}
}

void ctx::load_file(const char * file)
{
	int status;

	// load the file containing the script we are going to run
	status = luaL_loadfile(_L, file);
	switch (status) {
	case LUA_OK:
		std::cout << "LUA_OK: " << file << " loaded sucessfully" << std::endl;
		break;
	case LUA_ERRFILE:
		std::cout << "LUA_ERRFILE: " << lua_error(_L) << std::endl;
		break;
	case LUA_ERRSYNTAX:
		std::cout << "LUA_ERRSYNTAX: " << lua_error(_L) << std::endl;
		break;
	default:
		std::cout << lua_error(_L) << std::endl;
	}

	// call function once
	status = lua_pcall(_L, 0, 0, 0);  // 0 arguments, 0 results
	if (status != LUA_OK) {
		std::cout << "error running file" << lua_error(_L) << std::endl;
	}
}

void ctx::update()
{
	run(_u);
}

void ctx::reset()
{
	run(_r);
}

void ctx::set_value(int i, int j, float value)
{
	(*_m)(i, j) = value;
}

float ctx::get_value(int i, int j)
{
	return (*_m)(i, j);
}

float ctx::get_param_value(std::string k)
{
	float v = 0.5f;

	get_params("p");

	for (int i = 0; i < sizeof(_h); i++) {
		if (k == _h[i].key) {
			v = _h[i].value;
		}
	}
	return v;
}
