#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>

#include "overlay.h"

#define OFFSET 10

overlay::overlay(const char * file, int x, int y, int timer, int width)
{
	_x = x;
	_y = y;
	_timer = timer;
	_width = width;
	_surface = SDL_LoadBMP(file);
	if (NULL == _surface)
	{
		std::cout << "couldn't load image" << std::endl;
	}
}

overlay::~overlay()
{
	if (_surface)
	{
		SDL_FreeSurface(_surface);
	}
}

void overlay::render(int time)
{
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// create texture
	GLuint tid = 0;
	int mode = GL_RGBA;
	glGenTextures(1, &tid);
	glBindTexture(GL_TEXTURE_2D, tid);

	glTexImage2D(GL_TEXTURE_2D, 0, mode, _surface->w, _surface->h, 0, mode, GL_UNSIGNED_BYTE, _surface->pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex3f(_x, _y, 0);
		glTexCoord2f(1, 0); glVertex3f(_x + _surface->w, _y, 0);
		glTexCoord2f(1, 1); glVertex3f(_x + _surface->w, _y + _surface->h, 0);
		glTexCoord2f(0, 1); glVertex3f(_x, _y + _surface->h, 0);
	glEnd();

	_progress = (int) (_width * ((float)time / (float)_timer));
	glColor3f(0.5f, 0.5f, 0.5f);
	glBegin(GL_QUADS);
		glVertex2f(0, 0);
		glVertex2f(_progress, 0);
		glVertex2f(_progress, 10);
		glVertex2f(0, 10);
	glEnd();

	// restore context
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
}
