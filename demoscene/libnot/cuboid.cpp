#include <array>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "cuboid.h"

#define HI 0.2f
#define LO 0.0f

static int edges[12][2] =
{
	{0,1}, {1,2}, {2,3}, {3,0}, {4,5}, {5,6},
	{6,7}, {7,4}, {0,4}, {1,5}, {2,6}, {3,7}
};

static int surfaces[6][4] =
{
	{0,1,2,3}, {5,4,7,6}, {4,0,3,7}, {1,5,6,2}, {4,5,1,0}, {3,2,6,7}
};

// constructor
cuboid::cuboid(std::array<float, 3> size, float color, bool lines)
{
	_lines = lines;
	_offset = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI - LO)));

	update(size, color);
}

cuboid::~cuboid() {}

void cuboid::reset()
{
	_offset = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI - LO)));
	_size_x = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI - LO)));
	_size_y = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI - LO)));
	_size_z = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI - LO)));
}

void cuboid::update(std::array<float, 3> size, float color)
{
	_size_x  = size[0];
	_size_y  = size[1];
	_size_z  = size[2];
	_color = color;

	float sz_x = _size_x / 2 + _offset;
	float sz_y = _size_y / 2 + _offset;
	float sz_z = _size_z / 2 + _offset;

	// temporary vertex
	float vertex[8][3] =
	{
		{-sz_x, -sz_y, -sz_z},
		{sz_x, -sz_y, -sz_z},
		{sz_x, sz_y, -sz_z},
		{-sz_x, sz_y, -sz_z},
		{-sz_x, -sz_y, sz_z},
		{sz_x, -sz_y, sz_z},
		{sz_x, sz_y, sz_z},
		{-sz_x, sz_y, sz_z}
	};

	// temporary colors
	float colors[8][3] =
	{
		{color - _offset, color - _offset, color - _offset},
		{color - _offset, color - _offset, color - _offset},
		{color + _offset, color + _offset, color + _offset},
		{color + _offset, color + _offset, color + _offset},
		{color + _offset, color - _offset, color + _offset},
		{color + _offset, color - _offset, color + _offset},
		{color, color, color + _offset},
		{color, color, color + _offset}
	};

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			_vertex[i][j] = vertex[i][j];
			_colors[i][j] = colors[i][j];
		}
	}
}

void cuboid::render_full()
{
	glBegin(GL_QUADS);

	for (int i = 0; i < 6; i++)
	{
		glColor3fv(_colors[surfaces[i][0]]);
		glVertex3fv(_vertex[surfaces[i][0]]);
		glColor3fv(_colors[surfaces[i][1]]);
		glVertex3fv(_vertex[surfaces[i][1]]);
		glColor3fv(_colors[surfaces[i][2]]);
		glVertex3fv(_vertex[surfaces[i][2]]);
		glColor3fv(_colors[surfaces[i][3]]);
		glVertex3fv(_vertex[surfaces[i][3]]);
	}

	glEnd();
}

void cuboid::render_lines()
{
	glBegin(GL_LINES);

	for (int i = 0; i < 12; i++)
	{
		glColor3f(0.5f, 0.5f, 0.5f);
		glVertex3fv(_vertex[edges[i][0]]);
		glVertex3fv(_vertex[edges[i][1]]);
	}

	glEnd();
}

void cuboid::set_coords(float x, float y, float z)
{
	_coords[0] = x;
	_coords[1] = y;
	_coords[2] = z;
}

void cuboid::set_velocity(float v)
{
	_velocity = v;
}

float cuboid::get_velocity()
{
	return _velocity;
}

void cuboid::set_offset(float offset)
{
	_offset = offset;
}

std::array<float, 3> cuboid::get_size()
{
	return {_size_x, _size_y, _size_z};
}

void cuboid::render()
{
	glTranslatef(_coords[0], _coords[1], _coords[2]);
	glColor3ub(0, 255, 0);

	if (_lines)
		render_lines();
	else
		render_full();
}
