#ifndef __OVERLAY__
#define __OVERLAY__

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

class overlay
{
	public:
		overlay(const char * file, int x, int y, int timer, int width);
		~overlay();
		void render(int time);

	private:
		SDL_Surface * _surface;
		int _x;
		int _y;
		int _timer;
		int _width;
		int _progress = 100;
};

#endif  // __LOGO__
