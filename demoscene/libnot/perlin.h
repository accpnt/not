#ifndef __PERLIN_H__
#define __PERLIN_H__

class perlin
{
	public:
		perlin();
		~perlin();
		float noise(float sample_x, float sample_y, float sample_z);

	private:
		/* Permutation table */
		int p[256];

		/* Gradient vectors */
		float Gx[256];
		float Gy[256];
		float Gz[256];
};

#endif  // __PERLIN_H__
