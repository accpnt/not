#ifndef __CUBOID__
#define __CUBOID__

#include <string>
#include <vector>

class cuboid
{
	public:
		cuboid(std::array<float, 3>, float color, bool lines);
		~cuboid();
		void update(std::array<float, 3>, float color);
		void set_coords(float x, float y, float z);
		float get_velocity();
		void set_velocity(float v);
		void set_offset(float offset);
		std::array<float, 3> get_size();
		void reset();
		void render();

	private:
		void render_full();
		void render_lines();
		void render_points();
		float _colors[8][3];
		float _vertex[8][3];
		float _coords[3];
		float _size_x;
		float _size_y;
		float _size_z;
		float _color;
		float _offset;
		bool _lines;
		float _velocity;
};

#endif  // __CUBOID__
