#ifndef __MATRIX_CPP__
#define __MATRIX_CPP__

#include "matrix.h"

// Parameter Constructor
template<typename T>
matrix<T>::matrix(unsigned rows, unsigned cols, const T& initial) {
	_mat.resize(rows);

	for (unsigned i=0; i < _mat.size(); i++) {
			_mat[i].resize(cols, initial);
	}
	_rows = rows;
	_cols = cols;
}

// Copy Constructor
template<typename T>
matrix<T>::matrix(const matrix<T>& rhs) {
	_mat = rhs.mat;
	_rows = rhs.get_rows();
	_cols = rhs.get_cols();
}

// (Virtual) Destructor
template<typename T>
matrix<T>::~matrix() {
}

// Assignment Operator
template<typename T>
matrix<T>& matrix<T>::operator=(const matrix<T>& rhs) {
	if (&rhs == this)
		return *this;

	unsigned new_rows = rhs.get_rows();
	unsigned new_cols = rhs.get_cols();

	_mat.resize(new_rows);
	for (unsigned i=0; i < _mat.size(); i++) {
		_mat[i].resize(new_cols);
	}

	for (unsigned i=0; i < new_rows; i++) {
		for (unsigned j=0; j < new_cols; j++) {
			_mat[i][j] = rhs(i, j);
		}
	}
	_rows = new_rows;
	_cols = new_cols;

	return *this;
}

// Addition of two matrices
template<typename T>
matrix<T> matrix<T>::operator+(const matrix<T>& rhs) {
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i < _rows; i++) {
		for (unsigned j=0; j < _cols; j++) {
			result(i,j) = this->_mat[i][j] + rhs(i,j);
		}
	}

	return result;
}

// Cumulative addition of this matrix and another
template<typename T>
matrix<T>& matrix<T>::operator+=(const matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();

	for (unsigned i=0; i<rows; i++) {
		for (unsigned j=0; j<cols; j++) {
			this->_mat[i][j] += rhs(i,j);
		}
	}

	return *this;
}

// Subtraction of this matrix and another
template<typename T>
matrix<T> matrix<T>::operator-(const matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i<rows; i++) {
		for (unsigned j=0; j<cols; j++) {
			result(i,j) = this->_mat[i][j] - rhs(i,j);
		}
	}

	return result;
}

// Cumulative subtraction of this matrix and another
template<typename T>
matrix<T>& matrix<T>::operator-=(const matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();

	for (unsigned i=0; i<rows; i++) {
		for (unsigned j=0; j<cols; j++) {
			this->_mat[i][j] -= rhs(i,j);
		}
	}

	return *this;
}

// Left multiplication of this matrix and another
template<typename T>
matrix<T> matrix<T>::operator*(const matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i<rows; i++) {
		for (unsigned j=0; j<cols; j++) {
			for (unsigned k=0; k<rows; k++) {
				result(i,j) += this->_mat[i][k] * rhs(k,j);
			}
		}
	}

	return result;
}

// Cumulative left multiplication of this matrix and another
template<typename T>
matrix<T>& matrix<T>::operator*=(const matrix<T>& rhs) {
	matrix result = (*this) * rhs;
	(*this) = result;
	return *this;
}

// Calculate a transpose of this matrix
template<typename T>
matrix<T> matrix<T>::transpose() {
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i < _rows; i++) {
		for (unsigned j=0; j < _cols; j++) {
			result(i,j) = this->_mat[j][i];
		}
	}

	return result;
}

// Matrix/scalar addition
template<typename T>
matrix<T> matrix<T>::operator+(const T& rhs) {
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i<_rows; i++) {
		for (unsigned j=0; j<_cols; j++) {
			result(i,j) = this->_mat[i][j] + rhs;
		}
	}

	return result;
}

// Matrix/scalar subtraction
template<typename T>
matrix<T> matrix<T>::operator-(const T& rhs) {
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i<_rows; i++) {
		for (unsigned j=0; j<_cols; j++) {
			result(i,j) = this->_mat[i][j] - rhs;
		}
	}

	return result;
}

// Matrix/scalar multiplication
template<typename T>
matrix<T> matrix<T>::operator*(const T& rhs) {
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i<_rows; i++) {
		for (unsigned j=0; j<_cols; j++) {
			result(i,j) = this->_mat[i][j] * rhs;
		}
	}

	return result;
}

// Matrix/scalar division
template<typename T>
matrix<T> matrix<T>::operator/(const T& rhs) {
	matrix result(_rows, _cols, 0.0);

	for (unsigned i=0; i<_rows; i++) {
		for (unsigned j=0; j<_cols; j++) {
			result(i,j) = this->_mat[i][j] / rhs;
		}
	}

	return result;
}

// Multiply a matrix with a vector
template<typename T>
std::vector<T> matrix<T>::operator*(const std::vector<T>& rhs) {
	std::vector<T> result(rhs.size(), 0.0);

	for (unsigned i=0; i<_rows; i++) {
		for (unsigned j=0; j<_cols; j++) {
			result[i] = this->_mat[i][j] * rhs[j];
		}
	}

	return result;
}

// Obtain a vector of the diagonal elements
template<typename T>
std::vector<T> matrix<T>::diag_vec() {
	std::vector<T> result(_rows, 0.0);

	for (unsigned i=0; i<_rows; i++) {
		result[i] = this->_mat[i][i];
	}

	return result;
}


// Access the individual elements
template<typename T>
T& matrix<T>::operator()(const unsigned& row, const unsigned& col) {
	return this->_mat[row][col];
}

// Access the individual elements (const)
template<typename T>
const T& matrix<T>::operator()(const unsigned& row, const unsigned& col) const {
	return this->_mat[row][col];
}

// Get the number of rows of the matrix
template<typename T>
unsigned matrix<T>::get_rows() const {
	return this->_rows;
}

// Get the number of columns of the matrix
template<typename T>
unsigned matrix<T>::get_cols() const {
	return this->_cols;
}

#endif
