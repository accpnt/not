#ifndef __CTX__
#define __CTX__

#include <memory>
#include <lua.hpp>
#include "matrix.h"

class ctx
{
	typedef struct {
		std::string key;
		float value;
	} hash;

	public:
		ctx(const char * script, int rows, int columns);
		~ctx();
		void update();
		void reset();
		void set_value(int i, int j, float value);
		float get_value(int i, int j);
		float get_param_value(std::string k);

	private:
		void load_file(const char * file);
		void get_params(const char * p);
		void store();
		int retrieve_function(const char * function);
		void run(int fidx);
		lua_State * _L;
		int _idx;  // function index
		int _u;  // update function
		int _r;  // reset function
		hash _h[8];
		std::unique_ptr<matrix<float>> _m;
};

#endif  // __CTX__
