#ifndef __MATRIX__
#define __MATRIX__

#include <vector>

template <typename T> class matrix {
	public:
		matrix(unsigned rows, unsigned cols, const T& initial);
		matrix(const matrix<T>& rhs);
		virtual ~matrix();

		// Operator overloading, for "standard" mathematical matrix operations
		matrix<T>& operator=(const matrix<T>& rhs);

		// Matrix mathematical operations
		matrix<T> operator+(const matrix<T>& rhs);
		matrix<T>& operator+=(const matrix<T>& rhs);
		matrix<T> operator-(const matrix<T>& rhs);
		matrix<T>& operator-=(const matrix<T>& rhs);
		matrix<T> operator*(const matrix<T>& rhs);
		matrix<T>& operator*=(const matrix<T>& rhs);
		matrix<T> transpose();

		// Matrix/scalar operations
		matrix<T> operator+(const T& rhs);
		matrix<T> operator-(const T& rhs);
		matrix<T> operator*(const T& rhs);
		matrix<T> operator/(const T& rhs);

		// Matrix/vector operations
		std::vector<T> operator*(const std::vector<T>& rhs);
		std::vector<T> diag_vec();

		// Access the individual elements
		T& operator()(const unsigned& row, const unsigned& col);
		const T& operator()(const unsigned& row, const unsigned& col) const;

		// Access the row and column sizes
		unsigned get_rows() const;
		unsigned get_cols() const;

	private:
		std::vector<std::vector<T> > _mat;
		unsigned _rows;
		unsigned _cols;
};

#include "matrix.cpp"

#endif  // __MATRIX__
