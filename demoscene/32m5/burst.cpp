#include <iostream>
#include <array>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "burst.h"
#include "perlin.h"

#define HI 2.0f
#define LO 1.0f

#define PI 3.1416
#define MULT 10.0f

#define RESET_SMALL 0.01f
#define RESET_DEFAULT 1.0f

burst::burst(int nb_cubes, float size, float color, float offset)
{
	_nb_cubes = nb_cubes;
	_offset = offset;
	_direction = RESET_DEFAULT;

	_p = std::unique_ptr<perlin>(new perlin());
	_cubes.reset( new std::unique_ptr<cuboid>[nb_cubes] );
	_coords.reset(new float[nb_cubes * 3]);

	for(int i = 0; i < nb_cubes; i++) {
		bool lines = (bool)(rand() % 2);
		std::array<float, 3> asize = {size, size, size};
		_cubes[i].reset( new cuboid(asize, color, lines) );
		reset_coords(i, RESET_DEFAULT);
	}
}

float burst::transform(int i, float offset)
{
	float rho   = sqrt(_coords[3*i] * _coords[3*i] + _coords[3*i+1] * _coords[3*i+1] + _coords[3*i+2] * _coords[3*i+2]);
	float phi   = atan2(sqrt(_coords[3*i] * _coords[3*i] +  _coords[3*i+1] *  _coords[3*i+1]), _coords[3*i+2]);
	float theta = atan2(_coords[3*i+1], _coords[3*i]);

	rho += offset * _cubes[i]->get_velocity();

	_coords[3*i]   = rho * sin(phi) * cos(theta);
	_coords[3*i+1] = rho * sin(phi) * sin(theta);
	_coords[3*i+2] = rho * cos(phi);

	return rho;
}

void burst::explode(float offset)
{
	_rho = 0.0f;

	for (unsigned int i = 0; i < _nb_cubes; i++)
	{
		_rho += transform(i, _direction * offset);
	}

	_rho /= _nb_cubes;

	// change direction if the rho mean is too small
	if (_rho < 0.01) {
		_direction = -1.0f;
	}
}

void burst::reset_coords(int i, float mult)
{
	_coords[3*i]   = mult * _p->noise(rand() % 250 - 125, rand() % 250 - 125, 0.72);
	_coords[3*i+1] = mult * _p->noise(rand() % 250 - 125, rand() % 250 - 125, 0.72);
	_coords[3*i+2] = mult * _p->noise(rand() % 250 - 125, rand() % 250 - 125, 0.72);
}

void burst::reset()
{
	for(unsigned int i = 0; i < _nb_cubes; i++) {
		reset_coords(i, MULT * RESET_DEFAULT);
		float velocity = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
		_cubes[i]->set_velocity(velocity);
		_cubes[i]->reset();
	}

	_rho = 0;
	_direction = RESET_DEFAULT;
}

void burst::update(float size, float color, float offset)
{
	_size   = size;
	_color  = color;
	_offset = offset;
}

void burst::render()
{
	for (unsigned int i = 0; i < _nb_cubes; i++)
	{
		glPushMatrix();
			_cubes[i]->set_coords(_coords[3*i], _coords[3*i+1], _coords[3*i+2]);
			std::array<float, 3> size = {_size, _size, _size};
			_cubes[i]->update(size, _color);
			_cubes[i]->render();
		glPopMatrix();
	}
}
