#include <iostream>
#include <math.h>
#include <SDL2/SDL.h>
#define GL_GLEXT_PROTOTYPES 1
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>

#include "overlay.h"
#include "burst.h"

#define WIDTH 800
#define HEIGHT 600

#define NB_CUBES 20
#define SIZE 0.75f
#define COLOR 1.0f

#define HI 1.0f
#define LO 0.8f

#define CLEAR_A 0.5f

#define T_RESET 5000

static SDL_Window * window = NULL;
static SDL_GLContext gl_context;

static bool done = false;
static float angle = 1.0f;
static int last_time_reset = 0;
static int last_time_increase = 0;
static float v1[3]= {1.0f, 1.0f, 1.0f};
static float v2[3]= {-1.0f, 1.0f, -1.0f};
static float v[3];
static float offset = 0.0f;
static float toggle = 0;
static bool reset = false;
static bool increase = false;
static float random_time = 0.0f;
static float clear_x = 0.9f;
static float clear_y = 0.9f;
static float clear_z = 0.9f;
int delta = 0;

#define LOGO "./assets/logo.bmp"

static burst * b = NULL;

int SDLCALL watch(void *userdata, SDL_Event* event) {

	if (event->type == SDL_APP_WILLENTERBACKGROUND) {
		done = 1;
	}

	return 1;
}

void init()
{
	glMatrixMode(GL_PROJECTION);                    // Select The Projection Matrix
	glLoadIdentity();                               // Reset The Projection Matrix
	glOrtho(-5, 5, -5, 5, -50, 50);
	glMatrixMode(GL_MODELVIEW);                     // Select The Modelview Matrix
	glLoadIdentity();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT3);

	glClearColor(clear_x, clear_y, clear_z, CLEAR_A);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
	glPointSize(5.0f);
	glEnable(GL_FOG);
	glFogf(GL_FOG_START, -500);
	glFogf(GL_FOG_END, 500);
	glFogf(GL_FOG_DENSITY, 0.005);
}

void update()
{
	int current_time = SDL_GetTicks();

	delta = current_time - last_time_reset;

	if (toggle)
	{
		v[0] = v1[0];
		v[1] = v1[1];
		v[2] = v1[2];
	}
	else
	{
		v[0] = v2[0];
		v[1] = v2[1];
		v[2] = v2[2];
	}

	// increase or decrease randomly
	if ((float)(last_time_increase + random_time) < (float)current_time) {
		last_time_increase = current_time;
		increase = !increase;
	}

	// reset
	if (last_time_reset + T_RESET < current_time) {
		last_time_reset = current_time;
		increase = (bool)(rand() % 2);
		random_time = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/T_RESET));
		angle = 1.0f;
		angle = 1.0f;
		offset = 0.0f;
		toggle = !toggle;
		reset = true;
		clear_x = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
		clear_y = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
		clear_z = LO + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(HI-LO)));
	}
	else
	{
		angle = angle + 0.00001f;
		if (increase)
			offset += 0.0005f;
		else
			offset -= 0.001f;
		reset = false;
	}
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor4ub(255, 255, 255, 255);

	glRotatef(angle, v[0], v[1], v[2]);

	if (reset)
		b->reset();

	b->update(SIZE, COLOR, offset);
	b->explode(offset);
	b->render();
}

int main(int argc,char *argv[])
{
	burst bb(NB_CUBES, SIZE, COLOR, 0.0f);
	b = &bb;
	overlay o(LOGO, 25, 525, T_RESET, WIDTH);

	window = SDL_CreateWindow("5bdj", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP);

	gl_context = SDL_GL_CreateContext(window);

	init();
	b->reset();

	SDL_AddEventWatch(watch, NULL);

	while(!done) {
		SDL_Event event;
		while( SDL_PollEvent(&event) ) {
			if((event.type == SDL_QUIT) || (SDLK_ESCAPE == event.key.keysym.sym)) {
				done = 1;
			}
		}

		// update parameters
		update();

		// clear the screen again , different color
		glClearColor(clear_x, clear_y, clear_z, 0.5f);
		glClear(GL_COLOR_BUFFER_BIT);

		// 3d
		render();

		// switch context
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();

		glOrtho(0.0, WIDTH, HEIGHT, 0.0, -1.0f, 1.0f);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

		// draw 2d overlay
		o.render(delta);

		// end 2d
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glEnable(GL_DEPTH_TEST);

		SDL_GL_SwapWindow(window);
	}

	SDL_DelEventWatch(watch, NULL);
	SDL_GL_DeleteContext(gl_context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
