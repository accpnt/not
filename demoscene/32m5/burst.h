#ifndef __BURST__
#define __BURST__

#include <string>
#include <memory>
#include "cuboid.h"
#include "perlin.h"

class burst
{
	public:
		burst(int nb_cubes, float size, float color, float offset);
		void update(float size, float color, float offset);
		void render();
		void explode(float offset);
		void reset();

	private:
		void reset_coords(int i, float mult);
		float transform(int i, float offset);
		unsigned int _nb_cubes;
		std::unique_ptr<std::unique_ptr<cuboid>[]> _cubes;
		std::unique_ptr<float[]> _coords;
		std::unique_ptr<perlin> _p;
		float _size;
		float _color;
		float _offset;
		float _rho;
		float _direction;
};

#endif  // __BURST__
