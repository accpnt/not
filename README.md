# not

## demoscene

C++ demoscene project for music videos. Uses a custom library written in OpenGL/C++/SDL2 (libnot) that defines 3D basic primitives, overlays and add Lua scripting capabilities for demo parameter tweaks. Some of the demos can be publicly viewed on Vimeo at the following link: [cynepcyber](https://vimeo.com/cynepcyber).
